const express = require("express");
const router = express.Router();

const TaskController = require('../controllers/taskController')
//Get all the tasks
router.get("/", (req, res) => {
	TaskController.getAllTasks().then(resultFromController => res.send(resultFromController))

})

//Creating a task
router.post("/", (req, res) => {
	TaskController.createTask(req.body).then(result => res.send(result));
})


//delete a task
//URL "http://localhost:3001/tasks/:id"
//params parameter
router.delete("/:id", (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send(result));
})


//update a task
router.put("/:id", (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})



//ACTIVITY++++++++++++

router.get("/:id", (req, res) => {
	TaskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


router.put("/:id/complete", (req,res) => {

	TaskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})




module.exports = router;



